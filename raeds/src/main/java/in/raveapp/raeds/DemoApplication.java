package in.raveapp.raeds;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoApplication {

  @GetMapping("/hi")
  public String hello() {
    return "hello world!";
  }
}